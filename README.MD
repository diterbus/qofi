# qofi
Coffee colors based theme for Sublime Text editor.

![Sublime Text](https://img.shields.io/badge/sublime_text-%23575757.svg?style=for-the-badge&logo=sublime-text&logoColor=important) [![GitHub forks](https://img.shields.io/github/forks/freakyelf/qofi?color=%23FF9800&logo=%23FF9800&logoColor=%23FF9800&style=for-the-badge)](https://github.com/freakyelf/qofi/network) [![GitHub stars](https://img.shields.io/github/stars/freakyelf/qofi?color=%23FF9800&logo=%23FF9800&logoColor=%23FF9800&style=for-the-badge)](https://github.com/freakyelf/qofi/stargazers) 
## Preview
![](https://i.imgur.com/SoRmQqL.png)

## How to install qofi

Clone this repistory `git clone https://github.com/diterbus/qofi/` or click code and download zip. Move qofi file to C:\Users\YourName\AppData\Roaming\Sublime Text 3\Packages create a folder for color schemes and move in color scheme folder. Click preferences > select color scheme > qofi


> Contact me

Discord: diter#7313
